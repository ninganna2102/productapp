﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;

namespace ProductApp.Droid.Views
{
    [Activity(Label = "ProductApp.Droid", MainLauncher = true)]
    public class SplashView : MvxSplashScreenActivity
    {
        public SplashView() : base(Resource.Layout.splash_view)
        {

        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }
    }
}