﻿using MvvmCross.ViewModels;
using ProductApp.Core.Product;
using ProductApp.Core.Product.Contract.Dto;
using System;
using System.Collections.Generic;

namespace ProductApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            ProductService productService = new ProductService();
            CategoryDto category;
            List<String> categories = new List<string>() { "Electronics", "Clothing", "Footwear" };
            foreach (var item in categories)
            {
                category = new CategoryDto();
                category.Id = Guid.NewGuid();
                category.Name = item;
                productService.StoreCategory(category);
            }
            var data = productService.GetAllCategories();
        }
    }
}
