﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.Contract.Dto
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryDto Category { get; set; }
        public long Price { get; set; }
        public Byte[] ImageData { get; set; }
    }
}