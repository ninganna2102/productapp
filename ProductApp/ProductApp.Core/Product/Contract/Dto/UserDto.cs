﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.Contract.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}
