﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.Contract.Dto
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
