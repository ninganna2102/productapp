﻿using ProductApp.Core.Product.Contract.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.Contract
{
    public interface IProductService
    {
        bool StoreUser(UserDto user);
        bool StoreCategory(CategoryDto category);
        bool StoreProduct(ProductDto product);
    }
}
