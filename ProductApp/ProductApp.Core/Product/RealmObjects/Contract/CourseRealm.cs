﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.RealmObjects.Contract
{
    public class CourseRealm : RealmObject
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Name { get; set; }
        public CategoryRealm ProductCategory { get; set; }
    }
}
