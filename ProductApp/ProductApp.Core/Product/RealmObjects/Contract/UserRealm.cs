﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Core.Product.RealmObjects.Contract
{
    public class UserRealm : RealmObject
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; } 
    }
}
