﻿using ProductApp.Core.Product.Contract;
using ProductApp.Core.Product.Contract.Dto;
using ProductApp.Core.Product.RealmObjects.Contract;
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductApp.Core.Product
{
    public class ProductService : IProductService
    {
        private RealmConfiguration _realmConfiguration;
        public ProductService()
        {
            _realmConfiguration = new RealmConfiguration
            {
                SchemaVersion = 1
            };
        }


        public bool StoreCategory(CategoryDto category)
        {
            try
            {
                var realm = Realms.Realm.GetInstance(_realmConfiguration);

                var c = realm.Find<CategoryRealm>(category.Id.ToString());

                if (c == null)
                {
                    c = new CategoryRealm();
                    c.Id = category.Id.ToString();
                    c.Name = category.Name;
                }
                realm.Write(() =>
                {
                    realm.Add(c, update: true);
                });
            }
            catch(Exception e)
            {
                return false;
            }

            return true;
            

        }

        public bool StoreProduct(ProductDto product)
        {
            return true;
        }

        public bool StoreUser(UserDto user)
        {
            return true;
        }

        public List<CategoryDto> GetAllCategories()
        {
            var realm = Realms.Realm.GetInstance(_realmConfiguration);

            

            List<CategoryDto> list = new List<CategoryDto>();

            

            try
            {
                var ans = realm.All<CategoryRealm>().ToList();
                foreach (var item in ans)
                {
                    var res = new CategoryDto();
                    res.Id = Guid.Parse(item.Id);
                    res.Name = item.Name;
                    list.Add(res);
                }

            }
            catch(Exception ex)
            {

            }

            return list;
        }
    }
}
